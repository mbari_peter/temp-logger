

/*
  SD card datalogger
 
 This example shows how to log data from three analog sensors 
 to an SD card using the SD library.
 	
 The circuit:
 * analog sensors on analog ins 0, 1, and 2
 * SD card attached to SPI bus as follows:
 ** MOSI - pin 11
 ** MISO - pin 12
 ** CLK - pin 13
 ** CS - pin 10
 
 created  24 Nov 2010
 modified 9 Apr 2012
 by Tom Igoe
 
 This example code is in the public domain.
 	 
 */
#include <SoftwareSerial.h>
#include <SD.h>
#include <Wire.h>
#include "RTClib.h"
RTC_DS1307 RTC;
#include <Metro.h>


// On the Ethernet Shield, CS is pin 4. Note that even if it's not
// used as the CS pin, the hardware CS pin (10 on most Arduino boards,
// 53 on the Mega) must be left as an output or the SD library
// functions will not work.

SoftwareSerial sim900(3,2);
const int chipSelect = 10;
char date[10];
char time[10]; 
int okLed=6;
int notokLed=5;

float temp1C, temp2C, temp3C, temp4C;
char temp1CBuffer[6];
char temp2CBuffer[6];
char temp3CBuffer[6];
char temp4CBuffer[6];

unsigned char gsmPw=7;

char dataString[150];
int sensor1Value,sensor2Value,sensor3Value,sensor4Value;

File dataFile;

Metro logInterval=Metro(9600);
#define filterL 0.95
#define filterS 0.05

long int counter;

void setup()
{

  Serial.begin(9600);
  sim900.begin(9600);
  Wire.begin();
  RTC.begin();
  analogReference(INTERNAL);
  //RTC.adjust(DateTime(__DATE__, __TIME__));

  if (! RTC.isrunning()) {
    digitalWrite(notokLed,HIGH);
    // Serial.println("RTC is NOT running!");
    // following line sets the RTC to the date & time this sketch was compiled
    RTC.adjust(DateTime(__DATE__, __TIME__));

  }

  //Serial.print("Initializing SD card...");
  // make sure that the default chip select pin is set to
  // output, even if you don't use it:
  pinMode(10, OUTPUT);
  pinMode(gsmPw,OUTPUT);

  power_on();
  sendATcommand("ATE0","OK",2000);
  while (sendATcommand("AT+CREG?", "+CREG: 0,1", 2000) == 0);

  if(connectGPRS())sendSms("0724540103","GPRS on");
  else sendSms("0724540103","GPRS OFF");


  if (!SD.begin(chipSelect)) {

    digitalWrite(notokLed,HIGH);

    //Serial.println("Card failed, or not present");
    // don't do anything more:
    //return;
  }
  File dataFile = SD.open("datalog.CSV", FILE_WRITE);
  dataFile.println("logCounter,timeStamp,sensor1,sensor2,sensor3,sensor4");
  //Serial.println("card initialized.");
  dataFile.close();
}


void loop()
{
  getTime();
  DateTime now;

  // read the input on analog pin 0:
  sensor1Value = analogRead(A0);
  delay(1);
  sensor2Value = analogRead(A1);
  delay(1);
  sensor3Value = analogRead(A2);
  delay(1);

  sensor4Value = analogRead(A3);
  delay(1);

  //tempC=(((sensor2Value*(5000/1024)-0))/10)
  //float rawTemp=(((sensor1Value*(5000/1024)-0))/10);
  //float rawTemp=0.4831*sensor1Value-0.7397;
  float rawTemp=sensor1Value/9.31;

  temp1C=(filterS*rawTemp)+(filterL*temp1C);

  float rawTemp2=sensor2Value/9.31;
  // float rawTemp2=(((sensor2Value*(5000/1024)-0))/10);
  temp2C=(filterS*rawTemp2)+(filterL*temp2C);

  float rawTemp3=sensor3Value/9.31;
  //float rawTemp3=(((sensor3Value*(5000/1024)-0))/10);
  temp3C=(filterS*rawTemp3)+(filterL*temp3C);


  float rawTemp4=sensor4Value/9.31;
  //float rawTemp4=(((sensor4Value*(5000/1024)-0))/10);
  temp4C=(filterS*rawTemp4)+(filterL*temp4C);

  if(logInterval.check()){
    Serial.println(temp1C,1);
    Serial.println(temp2C,1);
    Serial.println(temp3C,1);
    Serial.println(temp4C,1);


    // open the file. note that only one file can be open at a time,
    // so you have to close this one before opening another.
    File dataFile = SD.open("datalog.CSV", FILE_WRITE);

    // if the file is available, write to it:
    if (dataFile) {
      dataFile.print(counter);
      dataFile.print( ",");
      counter++;
      dataFile.write('"');
      dataFile.print(date);
      dataFile.print( " ");
      dataFile.print(time);
      dataFile.write('"');
      dataFile.print( ",");
      dataFile.print(temp1C,1);
      dataFile.print( ",");
      dataFile.print(temp2C,1);
      dataFile.print( ",");
      dataFile.print(temp3C,1);
      dataFile.print( ",");
      dataFile.print(temp4C,1);
      dataFile.print( ",");
      dataFile.print(sensor1Value);
      dataFile.print( ",");
      dataFile.print(sensor2Value);
      dataFile.print( ",");
      dataFile.print(sensor3Value);
      dataFile.print( ",");
      dataFile.print(sensor4Value);
      dataFile.println();
      dataFile.close();
      digitalWrite(okLed,HIGH);
      delay(500);
      digitalWrite(okLed,LOW);
      delay(500);
      digitalWrite(okLed,HIGH);


    }
    // if the file isn't open, pop up an error:
    else {
      Serial.println("error opening datalog.CSV");
    } 
  }  
}


void getTime(){
  memset(date,'\0',10);
  memset(time,'\0',10);
  DateTime now = RTC.now();
  sprintf(date,"%d-%d-%d",now.day(),now.month(),now.year());
  sprintf(time,"%d:%d:%d",now.hour(), now.minute(), now.second());
}




boolean power_on(){
  uint8_t answer=0;
  digitalWrite(gsmPw,HIGH);
  delay(3000);
  digitalWrite(gsmPw,LOW);
  // checks if the module is started
  delay(3000);
  answer = sendATcommand("AT", "OK", 2000);
  if(answer==1)sendATcommand("AT", "OK", 2000);// turn off the echo
  else if (answer == 0)
  {
    // power on pulse
    digitalWrite(gsmPw,HIGH);
    delay(3000);
    digitalWrite(gsmPw,LOW);

    // waits for an answer from the module
    int trials=0;
    while(answer == 0){  
      trials++;
      // Send AT every two seconds and wait for the answer   
      answer = sendATcommand("AT", "OK", 2000); 
      if(trials==5){
        Serial.println(F("Gsm Fail"));
        //break;
        return false;
      }
    }
    sendATcommand("ATE0", "OK", 2000);// turn off the echo
  }
  else if (answer==1)return true;
}



int8_t sendATcommand(char* ATcommand, char* expected_answer1, unsigned long timeout){
  uint8_t x=0,  answer=0;
  char response[50];
  unsigned long previous;

  memset(response, '\0', 50);    // Initialize the string

  delay(100);

  while( sim900.available() > 0) sim900.read();    // Clean the input buffer

  sim900.println(ATcommand);    // Send the AT command 
#ifdef dbg
  Serial.println(ATcommand);    // Send the AT command 
#endif
  x = 0;
  previous = millis();

  // this loop waits for the answer
  do{
    if(sim900.available() != 0){  
      if(x==sizeof(response)) break; 
      response[x] = sim900.read();
      x++;
      // check if the desired answer is in the response of the module
      if (strstr(response, expected_answer1) != NULL)    
      {
        answer = 1;
      }
    }
    // Waits for the asnwer with time out
  }
  while((answer == 0) && ((millis() - previous) < timeout));  
#ifdef dbg  
  Serial.println(response);    // Send the AT command 
#endif

  return answer;
}

char at_buffer[100];
boolean AIRTIMEERROR=false;

boolean sendSms(char* number,char* message){
  int answer;
  sendATcommand("AT+CMGF=1", "OK", 1000);    // sets the SMS mode to text
#ifdef dbg 
  Serial.println(F("SMS mode"));
  Serial.println(F("Sending SMS"));
#endif
  memset(at_buffer, '\0',sizeof(at_buffer));
  sprintf(at_buffer,"AT+CMGS=\"%s\"", number);
  answer = sendATcommand(at_buffer, ">", 2000);    // send the SMS number
  if (answer == 1)
  {
    sim900.println(message);
    sim900.write(0x1A);
    answer = sendATcommand("", "OK", 20000);
    if (answer == 1)
    {
#ifdef dbg
      Serial.println("Sent ");  
#endif
      AIRTIMEERROR=false;
      return true;  
    }
    else
    {
      Serial.println("error ");
      AIRTIMEERROR=true;
    }
  }
  else
  {
    Serial.print("error ");
    Serial.println(answer, DEC);
    return false;
  }


}


boolean connectGPRS(){
  int trials=0;
  while( sendATcommand("AT+SAPBR=3,1,\"Contype\",\"GPRS\"", "OK", 2000)==0){
    trials++;
    if(trials==3){
      return false;
    }
  }
  trials=0;
  while(  sendATcommand("AT+SAPBR=3,1,\"APN\",\"web.safaricom.com\"", "OK", 2000)==0){
    trials++;
    if(trials==3){
      return false;

    }
  }
  trials=0;
  while(sendATcommand("AT+SAPBR=3,1,\"USER\",\"web\"", "OK", 2000)==0){
    trials++;
    if(trials==3){
      return false;
    }
  }
  trials=0;
  while( sendATcommand("AT+SAPBR=3,1,\"PWD\",\"web\"", "OK", 2000)==0){
    trials++;
    if(trials==3){
      return false;
    }

  }
  trials=0;

  // gets the GPRS bearer
  while (sendATcommand("AT+SAPBR=1,1", "OK", 20000) == 0)
  { 
    delay(5000);
    trials++;
    if(trials==3){
      return false;
    }
  }
  return true;
}





