
/*
  SD card datalogger
 
 This example shows how to log data from three analog sensors 
 to an SD card using the SD library.
 	
 The circuit:
 * analog sensors on analog ins 0, 1, and 2
 * SD card attached to SPI bus as follows:
 ** MOSI - pin 11
 ** MISO - pin 12
 ** CLK - pin 13
 ** CS - pin 10
 
 created  24 Nov 2010
 modified 9 Apr 2012
 by Tom Igoe
 
 This example code is in the public domain.
 	 
 */

#include <SD.h>
#include <Wire.h>
#include "RTClib.h"
RTC_DS1307 RTC;
#include <Metro.h>


// On the Ethernet Shield, CS is pin 4. Note that even if it's not
// used as the CS pin, the hardware CS pin (10 on most Arduino boards,
// 53 on the Mega) must be left as an output or the SD library
// functions will not work.
const int chipSelect = 10;
char date[10];
char time[10]; 
int okLed=6;
int notokLed=5;

float temp1C, temp2C, temp3C, temp4C;
char temp1CBuffer[6];
char temp2CBuffer[6];
char temp3CBuffer[6];
char temp4CBuffer[6];

char dataString[150];
int sensor1Value,sensor2Value,sensor3Value,sensor4Value;

File dataFile;

Metro logInterval=Metro(5000);
#define filterL 0.95
#define filterS 0.05

long int counter;

void setup()
{

  Serial.begin(115200);
  Wire.begin();
  RTC.begin();
  analogReference(INTERNAL);
  //RTC.adjust(DateTime(__DATE__, __TIME__));

  if (! RTC.isrunning()) {
    digitalWrite(notokLed,HIGH);
    Serial.println("RTC is NOT running!");
    // following line sets the RTC to the date & time this sketch was compiled
    RTC.adjust(DateTime(__DATE__, __TIME__));

  }

  Serial.print("Initializing SD card...");
  // make sure that the default chip select pin is set to
  // output, even if you don't use it:
  pinMode(10, OUTPUT);



  // see if the card is present and can be initialized:
  if (!SD.begin(chipSelect)) {

    digitalWrite(notokLed,HIGH);

    Serial.println("Card failed, or not present");
    // don't do anything more:
    //return;
  }
  File dataFile = SD.open("datalog.CSV", FILE_WRITE);
  dataFile.println("logCounter,timeStamp,sensor1,sensor2,sensor3,sensor4");
  Serial.println("card initialized.");
  dataFile.close();
}


void loop()
{
  getTime();
  DateTime now;

  // read the input on analog pin 0:
  sensor1Value = analogRead(A0);
  delay(1);
  sensor2Value = analogRead(A1);
  delay(1);
  sensor3Value = analogRead(A2);
  delay(1);

  sensor4Value = analogRead(A3);
  delay(1);

  //tempC=(((sensor2Value*(5000/1024)-0))/10)
  //float rawTemp=(((sensor1Value*(5000/1024)-0))/10);
  //float rawTemp=0.4831*sensor1Value-0.7397;
  //float rawTemp=sensor1Value/9.31;
  //y = 0.1081x - 1.5428

   float rawTemp=0.1081*sensor1Value-1.5428;
  temp1C=(filterS*rawTemp)+(filterL*temp1C);

  //float rawTemp2=sensor2Value/9.31;
  float rawTemp2=0.1086*sensor2Value-0.8647;
  temp2C=(filterS*rawTemp2)+(filterL*temp2C);

  //float rawTemp3=sensor3Value/9.31;
float rawTemp3=0.1122*sensor3Value+1.2281;
  temp3C=(filterS*rawTemp3)+(filterL*temp3C);


  //float rawTemp4=sensor4Value/9.31;
  float rawTemp4=0.1131*sensor4Value+1.4762;
  //float rawTemp4=(((sensor4Value*(5000/1024)-0))/10);
  temp4C=(filterS*rawTemp4)+(filterL*temp4C);

  if(logInterval.check()){
    Serial.println(temp1C,1);
    Serial.println(temp2C,1);
    Serial.println(temp3C,1);
    Serial.println(temp4C,1);


    // open the file. note that only one file can be open at a time,
    // so you have to close this one before opening another.
    File dataFile = SD.open("datalog.CSV", FILE_WRITE);

    // if the file is available, write to it:
    if (dataFile) {
      dataFile.print(counter);
      dataFile.print( ",");
      counter++;
      dataFile.write('"');
      dataFile.print(date);
      dataFile.print( " ");
      dataFile.print(time);
      dataFile.write('"');
      dataFile.print( ",");
      dataFile.print(temp1C,1);
      dataFile.print( ",");
      dataFile.print(temp2C,1);
      dataFile.print( ",");
      dataFile.print(temp3C-3,1);
      dataFile.print( ",");
      dataFile.print(temp4C-4,1);
//      dataFile.print( ",");
//      dataFile.print(sensor1Value);
//      dataFile.print( ",");
//      dataFile.print(sensor2Value);
//      dataFile.print( ",");
//      dataFile.print(sensor3Value);
//      dataFile.print( ",");
//      dataFile.print(sensor4Value);
      dataFile.println();
      dataFile.close();
      digitalWrite(okLed,HIGH);
      delay(500);
      digitalWrite(okLed,LOW);
      delay(500);
      digitalWrite(okLed,HIGH);


    }
    // if the file isn't open, pop up an error:
    else {
      Serial.println("error opening datalog.CSV");
    } 
  }  
}


void getTime(){
  memset(date,'\0',10);
  memset(time,'\0',10);
  DateTime now = RTC.now();
  sprintf(date,"%d-%d-%d",now.day(),now.month(),now.year());
  sprintf(time,"%d:%d:%d",now.hour(), now.minute(), now.second());
}













